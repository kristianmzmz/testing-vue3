import {describe, it, expect, beforeEach, afterEach} from 'vitest'
import {mount} from '@vue/test-utils'
import MyModal from '../MyModal.vue'

describe('MyModal component test', () => {
    it('should render the component when mounted', () => {
        const modalComponent = mount(MyModal)
        expect(modalComponent.html()).toContain('Open Modal')
    });

    it('should change the open property to true when the button is clicked', () => {
        const modalComponent = mount(MyModal)
        const button = modalComponent.find('button')
        expect(modalComponent.vm.$data['open']).toBeFalsy()

        button.trigger('click')

        expect(modalComponent.vm.$data['open']).toBeTruthy()
    });

    it('should render the teleport section when the button to open the model is clicked', async () => {
        const modalComponent = mount(MyModal)
        const button = modalComponent.get('button')

        await button.trigger('click')

        expect(document.body.outerHTML).toContain('Hello from the modal!')
    });
})
